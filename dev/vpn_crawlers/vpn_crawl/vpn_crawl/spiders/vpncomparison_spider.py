import scrapy
from scrapy.selector import Selector
from scrapy.http import HtmlResponse
from collections import Counter
import json


global_vpns = []


class vpnmentorSpider(scrapy.Spider):
	name = "vpncomparison"

	def start_requests(self):
		url = "http://www.vpncomparison.org/country/"
		yield scrapy.Request(url=url, callback=self.parse_list)

	#callback function to parse all VPNs by countries
	def parse_list(self, response):
		try:
			countries = response.selector.css('a[class=country]::attr(href)').extract()
			for country_link in countries:
				country=country_link[:-1].split("/")[-1]
				yield scrapy.Request(url=country_link, callback=self.parse_page, meta={'country': country})

		except Exception as e:
			print("*"*25, e,'Error at list page at for VPN:')


	#callback function to parse the individual Counrty VPN page, will contain duplicates
	def parse_page(self, response):

		global global_vpns

		ref_country = response.meta.get('country')
		rec_vpns = response.selector.css('.row.tbody.providerrow')

		vpn_links = response.selector.css('a[class=details-link]::attr(href)').extract()
		vpn_list = []

		# # get VPN per country
		# for vpn in rec_vpns:
		# 	review_link = vpn.css('a::attr(href)').extract()[0]
		# 	vpn_name = vpn.css('a::attr(href)').extract()[0].split("/")[-1].replace('.com','')
		# 	vpn_list.append(vpn_name)
		
		# with open('vpn_by_country.json', 'a+') as fp:
		# 	json.dump(dict(country=ref_country, vpns=vpn_list), fp)
		# 	fp.write("\n")

		for vpn_link in vpn_links:
			vpn_name = vpn_link.split("/")[-1].replace('.com','')

			if vpn_link not in global_vpns:
				global_vpns.append(vpn_link)
				yield scrapy.Request(url=vpn_link, callback=self.parse_vpn, meta = {'name': vpn_name})


	def parse_vpn(self, response):
		try:

			name = response.meta.get('name')
			info = response.selector.css('.provider-details.boxwrap.box')
			
			ratings = info.css('.stars .no::text').extract()
			details = [x.lower() for x  in info.css('.details-box.details li::text').extract()]
			encryption = [x.lower().replace("\t","").replace("\n","") for x in info.css('.details-box.encryption::text').extract()[1:]]
			protocols = [x.lower().replace("\t","").replace("\n","") for x in info.css('.details-box.protocols::text').extract()[1:]]
			bestuse = [x.replace("\r","").lower() for x in info.css('.details-box.bestuse li::text').extract()]
			prices = [x.lower() for x in info.css('.details.prices li::text').extract()]
			payment = [x.lower().replace("\t","").replace("\n","").strip(" ") for x in info.css('.details-box.payment::text').extract()[1].split(",")]
			features = [x.replace("\r","").lower() for x in info.css('.details-box.features li::text').extract()]
			clients = [x.lower().replace("\t","").replace("\n","") for x in info.css('.details-box.features::text').extract()[4:]]
			pros = [x.replace("\r","").lower() for x in info.css('.pro li::text').extract()]
			cons = [x.replace("\r","").lower() for x in info.css('.contra li::text').extract()]

			vpn_entry = dict(name=name, web_rating=ratings[0], user_rating =ratings[1], details=details, encryption=encryption,
							protocols=protocols, suggested_uses=bestuse, price=prices, payment_methods=payment, features=features,
							clients=clients, pros=pros, cons=cons)

			with open('vpn_comparison.json', 'a+') as fp:
				json.dump(vpn_entry, fp)
				fp.write("\n")

		except Exception as e:
			print("*"*25, e,'Error at individual page at for VPN: %s' % name, "*"*25)
