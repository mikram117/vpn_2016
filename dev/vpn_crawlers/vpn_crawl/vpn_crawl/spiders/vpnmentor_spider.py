import scrapy
from scrapy.selector import Selector
from scrapy.http import HtmlResponse
from collections import Counter
import json

class vpnmentorSpider(scrapy.Spider):
	name = "vpnmentor"
	max_page = 53

	#url generator
	def URLGen(self): 
			cur_page = 1
			while cur_page <= self.max_page:
				yield "https://www.vpnmentor.com/bestvpns/overall/page/%d/" % (cur_page)
				cur_page +=1

	def start_requests(self):
		urlgen = self.URLGen() #create generator instance

		#yeild request objects
		for url in urlgen:
			yield scrapy.Request(url=url, callback=self.parse_list)

	#callback function to parse the list based VPN response
	def parse_list(self, response):	
		try:
			count = 0
			head = response.selector.css('.review-head')
			content = response.selector.css('.review-content')
			foot = response.selector.css('.review-foot')

			for h, c, f in zip(head, content, foot):

				cost = None
				setup_time = None
				head_params = h.css('div[class=parameters-wrap] strong::text').extract()

				if len(head_params) ==2:
					cost = head_params[0]
					setup_time = head_params[1]
				else:
					if 'minutes' in head_params[0]:
						setup_time = head_params[0]
					else:
						cost = head_params[0]


				vpn_name = h.css('div[class=image-wrap] img::attr(alt)').extract()[0]
				vpn_score = c.css('div[class=score-holder] strong::text').extract()[0]
				vpn_rank = int(''.join(e for e in h.css('div[class=text-block] strong::text').extract()[0] if e.isdigit()))
				vpn_desc = c.css('ul>li::text').extract()

				#mobile compatability
				if len(f.css('h6[class=check-v]').extract()) > 0:
					vpn_mobile = True
				else:
					vpn_mobile = False

				#money back days
				guarante = f.css('h5[class=guarantee]+h6::text').extract()
				if len(guarante) > 0:
					moneyback = int(guarante[0])
				else:
					moneyback = 0

				url = f.css('h6>a>strong::text').extract()[0]
				follow_link = c.css('div[class=score-holder] a::attr(href)').extract()[0]

				vpn_entry = dict(name=vpn_name, overall_rating=float(vpn_score), rank=vpn_rank, url=url, cost=cost,
					setup_time=setup_time, features=vpn_desc, moible_app=vpn_mobile, guarantee_days=int(moneyback))

				yield scrapy.Request(url=follow_link, callback=self.parse_page, meta={'vpn_data': vpn_entry})
		
		except Exception as e:
			print("*"*25, e,'Error at list page at for VPN: %s' % vpn_name, "*"*25)

	#callback function to parse the individual VPN page
	def parse_page(self, response):

		try:

			vpn_protocols = ['openvpn', 'sstp', 'pptp', 'ipsec', 'ssh', 'l2tp', 'tls', 'ikev2']
			used_protocol =set()

			vpn_entry = response.meta.get('vpn_data')

			countries = 0
			servers = 0
			ip_addrs = None
			logs = ''
			kill_switch = False
			devices_per_line =1

			rating_content = response.selector.css('.rating-content')
			ratings = rating_content.css('td[class=dir_rlt] strong::text').extract()
			reviews_block = response.selector.css('.block-content')

			expert_text = response.selector.css('.expert-text-wrap')

			expert_review = ' '.join(expert_text.css('p').extract()).lower()
			
			for ptcl in vpn_protocols:
				if ptcl in expert_review:
					used_protocol.update([ptcl])

			feature_rating = float(ratings[0])
			useability_rating = float(ratings[1])
			price_rating = float(ratings[2])
			support_rating = float(ratings[3])

			feature_table = response.selector.css('table')[-1]
			feature_details = feature_table.css('td::text').extract()

			for row in range(0,len(feature_details), 2):

				if 'countries' in feature_details[row]: countries = int(feature_details[row+1].strip("\n"))
				if 'servers' in feature_details[row]: servers = int(feature_details[row+1].strip("\n"))
				if 'IP Addresses' in feature_details[row]: ip_addrs = int(feature_details[row+1].strip("\n"))
				if 'logs' in feature_details[row]: logs = feature_details[row+1].lower().strip("\n").strip("\t")
				if 'Kill' in feature_details[row] and feature_details[row+1].lower().strip("\n") == 'yes': kill_switch = True
				if 'license' in feature_details[row]: devices_per_line = feature_details[row+1]

			languages = reviews_block.css('div::attr(data-language)').extract()
			language_count = dict(Counter(languages))

			#update sub-ratings into dataset
			vpn_entry['feature_rating'] = feature_rating
			vpn_entry['useability_rating'] = useability_rating
			vpn_entry['price_rating'] = price_rating
			vpn_entry['support_rating'] = support_rating

			#update more specific details about the VPN
			vpn_entry['server_countries'] = countries
			vpn_entry['sever_count'] = servers
			vpn_entry['ip_count'] = ip_addrs
			vpn_entry['logs'] = logs
			vpn_entry['kill_switch'] = kill_switch
			vpn_entry['languages'] = language_count
			vpn_entry['protocols'] = list(used_protocol)


			with open('vpnmentor.json', 'a+') as fp:
				json.dump(vpn_entry, fp)
				fp.write("\n")
		
		except Exception as e:
			print("*"*25, e,'Error at individual page at for VPN: %s' % vpn_entry['name'], "*"*25)

